﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Math 
{

    //Return the number of gridCell to travel if there was no wall and holes, between g1 and g2.
    public static int CellDistance(GridCell g1, GridCell g2){
        return (Mathf.Abs(g1.getX() - g2.getX()) + Mathf.Abs(g1.getZ() - g2.getZ())); 
    }

    //Check whether the line between g1 and g2 can be drawn without intersecting a wall, using Bresenham's line algorithm
    public static bool drawLine(GridCell g1, GridCell g2){
      int x1 = g1.getX();
      int x2 = g2.getX();
      int z1 = g1.getZ();
      int z2 = g2.getZ();

      int dx = x2 - x1;
      int dz = z2 - z1;

      int x = x1;
      int z = z1;
      GridCell g = g1;

      if (dx != 0){
        if (dx > 0){
          if (dz != 0){
            if (dz > 0){
              if (dx >= dz){ //1
                int e = dx;
                dx = 2*e;
                dz = 2*dz;
                x++;
                if (!g.hasRight()){
                  return false;
                }
                g = g.getRight();
                while(x != x2){
                  e -= dz;
                  if (e <= 0){
                    z ++;
                    e += dx;
                    if (!g.hasUp()){
                      return false;
                    }
                    g = g.getUp();
                  }
                  x++;
                  if (!g.hasRight()){
                    return false;
                  }
                  g = g.getRight();
                }
              }


              else{ //2
                int e = dz;
                dz = e*2;
                dx = dx*2;
                z++;
                if (!g.hasUp()){
                  return false;
                }
                g = g.getUp();
                while(z != z2){
                  e = e - dx;
                  if (e <= 0){
                    x ++;
                    e += dz;
                    if (!g.hasRight()){
                      return false;
                    }
                    g = g.getRight();
                  }
                  z++;
                  if (!g.hasUp()){
                    return false;
                  }
                  g = g.getUp();
                }
              }
            } 


        
            else{ //dz < 0


              if (dx >= -dz){ //3
                int e = dx;
                dx = 2*e;
                dz = 2*dz;
                x++;
                if (!g.hasRight()){
                  return false;
                }
                g = g.getRight();
                while (x != x2){
                  e += dz;
                  if (e <= 0){
                    z --;
                    e += dx;
                    if (!g.hasDown()){
                      return false;
                    }
                    g = g.getDown();
                  }
                  x++;
                  if (!g.hasRight()){
                    return false;
                  }
                  g = g.getRight();
                }
              }
              


              else{ //4
                int e = dz;
                dz = 2*e;
                dx = 2*dx;
                z --;
                if (!g.hasDown()){
                  return false;
                }
                g = g.getDown();
                while(z != z2){
                  e += dx;
                  if (e >= 0){
                    x ++;
                    e += dz;
                    if (!g.hasRight()){
                      return false;
                    }
                    g = g.getRight();
                  }
                  z --;
                  if (!g.hasDown()){
                    return false;
                  }
                  g = g.getDown();
                }
              }
            }
          }

          else { // dz = 0
            while (x != x2){
              x++;
              if (!g.hasRight()){
                return false;
              }
              g = g.getRight();
            }
          }
        }

        else { //dx < 0
          if (dz != 0){ //5
            if (dz > 0){
              if (-dx >= dz){
                int e = dx;
                dx = 2*e;
                dz = 2*dz;
                x --;
                if (!g.hasLeft()){
                  return false;
                }
                g = g.getLeft();
                while (x != x2){
                  e += dz;
                  if (e >= 0){
                    z++;
                    e += dx;
                    if (!g.hasUp()){
                      return false;
                    }
                    g = g.getUp();
                  }
                  x --;
                  if (!g.hasLeft()){
                    return false;
                  }
                  g = g.getLeft();
                }
              }



              else{ //6
                int e = dz;
                dz = 2*e;
                dx = 2*dx;
                z++;
                if (!g.hasUp()){
                  return false;
                }
                g = g.getUp();
                while (z != z2){
                  e += dx;
                  if (e <= 0){
                    x --;
                    e += dz;
                    if (!g.hasLeft()){
                      return false;
                    }
                    g = g.getLeft();
                  }
                  z++;
                  if (!g.hasUp()){
                    return false;
                  }
                  g = g.getUp();
                }
              }
            }

            else{ // dz < 0
              if (dx <= dz){
                int e = dx;
                dx = 2*e;
                dz = 2*dz;
                x --;
                if (!g.hasLeft()){
                  return false;
                }
                g = g.getLeft();
                while(x != x2){
                  e -= dz;
                  if (e >= 0){
                    z-- ;
                    e += dx;
                    if (!g.hasDown()){
                      return false;
                    }
                    g = g.getDown();
                  }
                  x --;
                  if (!g.hasLeft()){
                    return false;
                  }
                  g = g.getLeft();
                }
              }
              else{
                int e = dz;
                dz = e * 2;
                dx = 2 * dx;
                z --;
                if (!g.hasDown()){
                  return false;
                }
                g = g.getDown();
                while(z != z2){
                  e = e - dx;
                  if (e >= 0){
                    x -- ;
                    e += dz;
                    if (!g.hasLeft()){
                      return false;
                    }
                    g = g.getLeft();
                  }
                  z --;
                  if (!g.hasDown()){
                    return false;
                  }
                  g = g.getDown();
                }
              }
            }
          }
          else{ //dz = 0
            while (x != x2){
              x --;
              if (!g.hasLeft()){
                return false;
              }
              g = g.getLeft();
            }
          }
        }
      }
      else{ //dx = 0
        if(dz < 0){
          while(z != z2){
            z--;
            if (!g.hasDown()){
              return false;
            }
            g = g.getDown();
          }
        }
        else{
          while(z != z2){
            z++;
            if (!g.hasUp()){
              return false;
            }
            g = g.getUp();
          }
        }
      }
      return true;
    }
}
