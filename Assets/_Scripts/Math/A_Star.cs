﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class A_Star
{
    private static int Compare(Cell c1, Cell c2)
    {
        if (c1.h + c1.cost > c2.h + c2.cost)
            return 1;
        else if ((c1.h + c1.cost) == (c2.h + c2.cost))
            return 0;
        else 
            return -1;
    }

    private class Cell
    {
        public GridCell gridCell;
        public int cost;
        public int h;
        

        public Cell(GridCell gridCell, GridCell goal, int cost){
            this.gridCell = gridCell;
            this.cost = cost;
            h = Math.CellDistance(gridCell, goal);
        }
    }
    private class CellComp : IComparer<Cell>
    {
        public int Compare(Cell c1, Cell c2) {
            if (c1.h + c1.cost > c2.h + c2.cost)
                return 1;
            else if ((c1.h + c1.cost) == (c2.h + c2.cost))
                return 0;
            else
                return -1;
        //return A_Star.Compare(c1, c2);
    }
    }

    private static void Auxi_(FilePrio<Cell> openList, Cell c, Cell neighbor, List<GridCell> closedList, Dictionary<GridCell, GridCell> cameFrom)
    {
        if (!openList.Contains(neighbor) && !closedList.Contains(neighbor.gridCell))
        {
            openList.Add(neighbor);
            if (cameFrom.ContainsKey(neighbor.gridCell))
                cameFrom.Remove(neighbor.gridCell);

            cameFrom.Add(neighbor.gridCell, c.gridCell);
        }
    }

    //Returns an empty list if a way is not found.
    public static List<GridCell> AStar(GridCell goal, GridCell start){

        List<GridCell> closedList = new List<GridCell>();
        FilePrio<Cell> openList = new FilePrio<Cell>(new CellComp());
        Dictionary<GridCell,GridCell> cameFrom = new Dictionary<GridCell,GridCell>();
        openList.Add(new Cell(start,goal,0));
        while(openList.getCount() > 0){
            //We take the cell with the lowest F-cost of the sorted set, and we remove it from it.
            Cell c = openList.Pop();
            //Debug.Log("OPEN Position " + c.gridCell.getX() + " " + c.gridCell.getZ() + " " + (c.h+c.cost));
            if ((Math.CellDistance(c.gridCell, goal) == 0)) //We check whether we've reached our destination
            {
                List<GridCell> way = findWay(c.gridCell, cameFrom, start);
                way.Reverse();
                way.Add(goal);
                return way; //If so, we return the way we computed
            }

           // openList.Remove(c);
            closedList.Add(c.gridCell);
            //We check every neighbour than can be reached.
            if (c.gridCell.hasUp()){
                Cell cUp = new Cell(c.gridCell.getUp(), goal, c.cost + 1);
               
                Auxi_(openList, c, cUp, closedList, cameFrom);
            }

            if (c.gridCell.hasDown()){
                Cell cDown = new Cell(c.gridCell.getDown(), goal, c.cost + 1);
                
                Auxi_(openList, c, cDown, closedList, cameFrom);
            }

            if (c.gridCell.hasLeft()){
                Cell cLeft = new Cell(c.gridCell.getLeft(), goal, c.cost + 1);
                
                Auxi_(openList, c, cLeft, closedList, cameFrom);
            }

            if (c.gridCell.hasRight()){
                Cell cRight = new Cell(c.gridCell.getRight(), goal, c.cost + 1);
               
                Auxi_(openList, c, cRight, closedList, cameFrom);
            }
            
        }
        Debug.Log("No way found");
        return new List<GridCell>();
    }

    private static List<GridCell> findWay(GridCell goal, Dictionary<GridCell, GridCell> cameFrom, GridCell start){
        List<GridCell> list = new List<GridCell>();
        GridCell current = goal;
        while (cameFrom.ContainsKey(current) && Math.CellDistance(cameFrom[current], start) != 0){
            current = cameFrom[current];
            list.Add(current);
        }
        return list;
    }
}

public class FilePrio<T>
{
    private List<T> File;
    IComparer<T> C;
    public FilePrio(IComparer<T> c)
    {
        File = new List<T>();
        C = c;
    }

    public void Add(T tuile)
    {
        int i = 0;
        foreach (T t in File)
        {
            if (C.Compare(tuile,t) < 0)
            {
                File.Insert(i, tuile);
                return;
            }
            i++;

        }
        File.Add(tuile);
    }

    public T Pop()
    {
        if (File.Count != 0)
        {
            var result = File[0];
            File.RemoveAt(0);
            return result;
        }
        Debug.Log("pop un file vide");
        throw new Exception("File vide");
    }

    public int getCount()
    {
        return File.Count;
    }

    //Fait pour A* Test directement si il existe un élément avec une heuristique + cout inférieurs
    public bool Contains(T t)
    {
        return File.Find(x => C.Compare(x, t) < 0) != null;
    }
}

