﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerP1 : Photon.MonoBehaviour
{
    Rigidbody rigi;
    // Start is called before the first frame update
    void Start()
    {
        rigi = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if(photonView.isMine)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                rigi.AddForce(100 * Vector3.up);
            }
        }
    }
}
