﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IControllable 
{
    public abstract bool MoveToward(GridCell target);

    public abstract bool MakeAction(int action);

}
