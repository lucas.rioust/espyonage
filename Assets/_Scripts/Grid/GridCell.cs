﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Wall{up, down, left, right};
public class GridCell : MonoBehaviour
{

    private void Start() {
        x = (int)transform.position.x;
        height = (int)transform.position.y;
        z = (int)transform.position.z;

        int layerMask = 1 << 9;
        RaycastHit hit;

        if (!hasWallUp() && Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, .6f, layerMask))
        {
            up = hit.transform.gameObject.GetComponent<GridCell>();
        }

        if (!hasWallDown() && Physics.Raycast(transform.position, transform.TransformDirection(-Vector3.forward), out hit, .6f, layerMask))
        {
            down = hit.transform.gameObject.GetComponent<GridCell>();
        }

        if (!hasWallRight() && Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, .6f, layerMask))
        {
            right = hit.transform.gameObject.GetComponent<GridCell>();
        }

        if (!hasWallLeft() && Physics.Raycast(transform.position, transform.TransformDirection(-Vector3.right), out hit, .6f, layerMask))
        {
            left = hit.transform.gameObject.GetComponent<GridCell>();
        }
    }

    int height;
    int x;
    int z;

    //The list of walls
    [SerializeField] List<Wall> walls;

    //The four possible GridCells surronding this one. Can be null
    GridCell up; 
    GridCell down;
    GridCell left;
    GridCell right;

    //The plane corresponding to the GridCell in the actual scene
    GameObject cell;

    //Defines if this GridCell can be walked on
    bool walkable;    

    //Defines whether the cell is lighted or not
    bool lighted;

    //Defines whether the cell is currently visible to the players
    bool visible;

    //The gameObject on the cell, if there is one. It can be a monster, a treasure ...
    GameObject onTheCell;

    public void setX(int x){
        this.x = x;
    }
    public int getX(){
        return x;
    }
    public void setZ(int z){
        this.z = z;
    }
    public int getZ(){
        return z;
    }
    public void setCell(GameObject cell){
        this.cell = cell;
    }
    public GameObject getCell(){
        return cell;
    }
    public void setWalkable(bool walkable){
        this.walkable = walkable;
    }
    public bool getWalkable(){
        return walkable;
    }
    public bool getLighted(){
        return lighted;
    }
    public void setLighted(bool lighted){
        this.lighted = lighted;
    }
    public void setOnTheCell(GameObject onTheCell){
        this.onTheCell = onTheCell;
    }
    public GameObject getOnTheCell(){
        return onTheCell;
    }
    public bool hasOnTheCell(){
        return (onTheCell != null);
    }





    public void setUp(GridCell up){
        this.up = up;
    }
    public bool hasUp(){
        return (up != null);
    }
    public GridCell getUp(){
        return up;
    }
    public void setDown(GridCell down){
        this.down = down;
    }
    public bool hasDown(){
        return (down != null);
    }
    public GridCell getDown(){
        return down;
    }
    public void setRight(GridCell right){
        this.right = right;
    }
    public bool hasRight(){
        return (right != null);
    }
    public GridCell getRight(){
        return right;
    }
    public void setLeft(GridCell left){
        this.left = left;
    }
    public bool hasLeft(){
        return (left != null);
    }
    public GridCell getLeft(){
        return left;
    }


    public bool hasUpLeft(){
        if ((hasUp() && getUp().hasLeft()) || (hasLeft() && getLeft().hasUp()))
            return true;
        return false;
    }
    public GridCell getUpLeft(){
        if (hasUp() && getUp().hasLeft())
            return getUp().getLeft();
        else
            return getLeft().getUp();
    }


    public bool hasUpRight(){
        if ((hasUp() && getUp().hasRight()) || (hasRight() && getRight().hasUp()))
            return true;
        return false;
    }
    public GridCell getUpRight(){
        if (hasUp() && getUp().hasRight())
            return getUp().getRight();
        else
            return getRight().getUp();
    }


    public bool hasDownRight(){
        if ((hasDown() && getDown().hasRight()) || (hasRight() && getRight().hasDown()))
            return true;
        return false;
    }
    public GridCell getDownRight(){
        if (hasDown() && getDown().hasRight())
            return getDown().getRight();
        else
            return getRight().getDown();
    }


    public bool hasDownLeft(){
        if ((hasDown() && getDown().hasLeft()) || (hasLeft() && getLeft().hasDown()))
            return true;
        return false;
    }
    public GridCell getDownLeft(){
        if (hasDown() && getDown().hasLeft())
            return getDown().getLeft();
        else
            return getLeft().getDown();
    }

    public bool hasWallUp(){
        return walls.Contains(Wall.up);
    }

    public bool hasWallDown(){
        return walls.Contains(Wall.down);
    }

    public bool hasWallRight(){
        return walls.Contains(Wall.right);
    }

    public bool hasWallLeft(){
        return walls.Contains(Wall.left);
    }

}